(1..100).to_a.each do |i|
  Product.create({
    name: "product #{i}",
    description: "description for product #{i}",
    price: 10*i
  })
end

(1..10).to_a.each do |i|
  Offer.create({
    name: "offer #{i}",
    description: "description for offer #{i}",
    start: Date.today - i.day,
    end: Date.today + (i+10).days,
    discount: 9*i
  })
end

(1..10).to_a.each do |i|
  cat = Category.find_or_create_by_name_and_description("category #{i}","description for category #{i}")
  cat.products = Product.all.shuffle.first(10)
  cat.save
end

bar = Branch.find_or_create_by_name("branch one")
bar.categories << Category.first(5)


products = Product.all
products_second_part = products.last(90)
products_second_part.each do |p|
  offers = Offer.all.shuffle
  OfferProduct.create({product_id: p.id,offer_id: offers.first.id})
end