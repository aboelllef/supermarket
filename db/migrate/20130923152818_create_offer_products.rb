class CreateOfferProducts < ActiveRecord::Migration
  def change
    create_table :offer_products do |t|
      t.integer :product_id
      t.integer :offer_id
      t.decimal :offer_price ,default: 0

      t.timestamps
    end
  end
end
