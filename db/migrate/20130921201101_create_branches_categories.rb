class CreateBranchesCategories < ActiveRecord::Migration
  def change
    create_table :branches_categories do |t|
      t.integer :branch_id
      t.integer :category_id
    end
  end
end
