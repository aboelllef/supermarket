class ChangeDateTimesToDates < ActiveRecord::Migration
  def change
    change_column :offers, :end, :date
    change_column :offers, :start, :date
  end
end
