class AddDiscountToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :discount, :integer, default: 0
  end
end
