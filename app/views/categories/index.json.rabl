collection @categories
attributes :id, :name, :description

child :products do 
  attributes :id, :name, :description, :price ,:has_offer
end