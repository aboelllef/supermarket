object @offer
attributes :id, :name, :description, :start ,:end


child :products do
  attributes :id, :name, :description, :price ,:has_offer
  node(:offer_price) {|pro| @offer.offer_price_of(pro.id)}
end