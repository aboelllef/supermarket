collection @offers
attributes :id, :name, :description, :start, :end

node :products do |offer|
  offer.products.map do |p|{
    id: p.id,
    name: p.name,
    description: p.description,
    price: p.price,
    has_offer: p.has_offer,
    offer_price: offer.offer_price_of(p.id) 
  }
  end
end