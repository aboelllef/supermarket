class OffersController < ApplicationController

  def index
    @offers = Offer.all
  end

  # GET /offers/1
  # GET /offers/1.json
  def show
    @offer = Offer.find(params[:id])
    @products = @offer.products
  end

  # GET /offers/new
  # GET /offers/new.json
  def new
    @offer = Offer.new({discount: nil})

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @offer }
    end
  end

  # GET /offers/1/edit
  def edit
    @offer = Offer.find(params[:id])
    @offer.start = @offer.start.to_date.strftime('%d/%m/%Y')
    @offer.end   = @offer.end.to_date.strftime('%d/%m/%Y')
    @products =@offer.products
    @unoffered_products = Product.all - @products
  end

  # POST /offers
  # POST /offers.json
  def create
    @offer = Offer.new(params[:offer])

    respond_to do |format|
      if @offer.save
        format.html { redirect_to "/offers/#{@offer.id}", notice: 'Offer was successfully created.' }
        format.json { render json: @offer, status: :created, location: @offer }
      else
        format.html { render action: "new" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /offers/1
  # PUT /offers/1.json
  def update
    @offer = Offer.find(params[:id])

    respond_to do |format|
      if @offer.update_attributes(params[:offer])
        format.html { redirect_to "/offers/#{@offer.id}", notice: 'Offer was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer = Offer.find(params[:id])
    @offer.destroy

    respond_to do |format|
      format.html { redirect_to offers_path}
      format.js
      format.json { head :no_content }
    end
  end
  

  def test_requests
    name = params[:user_name].blank? ? "nothing received " : params[:user_name] 
    respond_to do |format|
      format.html { render json: name }
      format.json { render json: name }
    end
  end
end
