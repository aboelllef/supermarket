class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :admin_user_signed_in? , except: [:index,:show]
  before_filter :force_json_for_scoped_links


  def remove_product
    @item = controller_name.classify.constantize.find params[:id]
    @product = Product.find(params[:product_id])
    @item.products.delete(@product)
  end

  def add_product
    @item = controller_name.classify.constantize.find params[:id]
    @product = Product.find(params[:product_id])
    @item.products << (@product)
  end


  private

    def force_json_for_scoped_links
      if params[:clientType] == 'api'
        request.format = :json
      end
    end

    def admin_user_signed_in?
      unless user_signed_in? && current_user.admin?
        redirect_to root_path,alert: "you need to be an admin to take this action"
      end
    end
end
