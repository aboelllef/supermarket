class Branch < ActiveRecord::Base
  attr_accessible :address, :name
  has_and_belongs_to_many :categories
  has_many :products, through: :categories
end
