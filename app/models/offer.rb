class Offer < ActiveRecord::Base
  attr_accessible :description, :end, :name, :start, :discount

  validates :name, presence: true
  validates :description, presence: true
  validates :start, presence: true
  validates :end, presence: true
  validates_numericality_of :discount,greater_than: 0,less_than_or_equal_to: 100
  

  has_many :branches
  has_many :offer_products
  has_many :products, through: :offer_products

  def offer_price_of product_id
    OfferProduct.where({product_id: product_id,offer_id: self.id}).first.try :offer_price
  end
  def discount
    if read_attribute(:discount).to_i == 0
      return nil
    else
      return read_attribute(:discount)
    end
  end

end
