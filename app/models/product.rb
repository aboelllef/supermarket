class Product < ActiveRecord::Base
  attr_accessible :description, :name, :price

  validates :name, presence: true
  validates :description, presence: true
  validates_numericality_of :price,greater_than: 0



  has_and_belongs_to_many :categories

  has_many :offer_products
  has_many :offers, through: :offer_products



  def branches
    branches = {}
    self.categories.each do |cat|
      branches[cat] = cat.branches
    end
    return branches
  end

  def has_offer
    !self.offers.empty?
  end

  def price_of_offer offer_id
    price = OfferProduct.where({product_id: self.id,offer_id: offer_id}).first.try :offer_price
    price.nil? ?  Product.find(self.id).price : price.round(2)
  end
end
