class OfferProduct < ActiveRecord::Base
  attr_accessible :offer_id, :offer_price, :product_id

  validates_uniqueness_of :offer_id, :scope => :product_id

  belongs_to :offer
  belongs_to :product


  def offer_price
    product = Product.find self.product_id
    offer = Offer.find self.offer_id
    price = product.price.to_i
    offered_price =price - price *(offer.discount/100.0)
    return offered_price
  end
end
