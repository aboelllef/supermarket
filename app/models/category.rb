class Category < ActiveRecord::Base
  attr_accessible :description, :name
  validates :name, presence: true
  validates :description, presence: true

  has_and_belongs_to_many :branches
  has_and_belongs_to_many :products

end
